# Dynamic MOTD

![sample](https://raw.githubusercontent.com/charlesrocket/motd/master/screenshot.png)

#### Dependencies
* `update-motd`
* `figlet`
* `lolcat`
#### Rebuild
`sudo /usr/sbin/update-motd`
